For QGroundControl installation in linux development with 18.04 use the version v4.0.11 from here: https://github.com/mavlink/qgroundcontrol/releases/tag/v4.0.11

For *ninja* errors with building 'make px4_sitl gazebo': use this commands to reinstall gazebo plugins:

```
sudo apt-get install libprotobuf-dev libprotoc-dev protobuf-compiler libeigen3-dev libxml2-utils python-rospkg python-jinja2
```
```
sudo apt-get install libgstreamer-plugins-base1.0-dev gstreamer1.0-plugins-bad gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-ugly -y
```
